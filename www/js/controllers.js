angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, CONSTANT) {
  $scope.CONSTANT = CONSTANT;
})

.controller('NewbiePlaylistCtrl', function($scope, $stateParams, NewbieFactory) {
  $scope.playlists = NewbieFactory;
  $scope.category = "Newbie";
})

.controller('NewbieCtrl', function($scope, $stateParams, $cordovaFileOpener2, NewbieFactory) {
  $scope.post = NewbieFactory[$stateParams.playlistId];
  $scope.playlists = NewbieFactory;

  $scope.openPDF = function() {
      console.log("/storage/sdcard1/Tagalog/Newbies/" + $scope.post.pdfLink);
      $cordovaFileOpener2.open(
          "/storage/sdcard1/Tagalog/Newbies/" + $scope.post.pdfLink, // Any system location, you CAN'T use your appliaction assets folder
          'application/pdf'
      ).then(function() {
          console.log('Success');
      }, function(err) {
          console.log('An error occurred: ' + JSON.stringify(err));
      });
  };

  $scope.openMp3 = function() {
      $cordovaFileOpener2.open(
          "/storage/sdcard1/Tagalog/Newbies/" + $scope.post.mp3Link, // Any system location, you CAN'T use your appliaction assets folder
          'audio/mpeg'
      ).then(function() {
          console.log('Success');
      }, function(err) {
          console.log('An error occurred: ' + JSON.stringify(err));
      });
  };
})
.controller('ElementaryPlaylistCtrl', function($scope, $stateParams, ElementaryFactory) {
  $scope.playlists = ElementaryFactory;
  $scope.category = "Elementary";
})

.controller('ElementaryCtrl', function($scope, $stateParams, $cordovaFileOpener2, ElementaryFactory) {
  $scope.post = ElementaryFactory[$stateParams.playlistId];
  $scope.playlists = ElementaryFactory;

  $scope.openPDF = function() {
      console.log("/storage/sdcard1/Tagalog/Elementary/" + $scope.post.pdfLink);
      $cordovaFileOpener2.open(
          "/storage/sdcard1/Tagalog/Elementary/" + $scope.post.pdfLink, // Any system location, you CAN'T use your appliaction assets folder
          'application/pdf'
      ).then(function() {
          console.log('Success');
      }, function(err) {
          console.log('An error occurred: ' + JSON.stringify(err));
      });
  };

  $scope.openMp3 = function() {
      $cordovaFileOpener2.open(
          "/storage/sdcard1/Tagalog/Elementary/" + $scope.post.mp3Link, // Any system location, you CAN'T use your appliaction assets folder
          'audio/mpeg'
      ).then(function() {
          console.log('Success');
      }, function(err) {
          console.log('An error occurred: ' + JSON.stringify(err));
      });
  };
})
.controller('PikaPikaPlaylistCtrl', function($scope, $stateParams, PikaPikaFactory) {
  $scope.playlists = PikaPikaFactory;
  $scope.category = "Pika-Pika";
})

.controller('PikaPikaCtrl', function($scope, $stateParams, $cordovaFileOpener2, PikaPikaFactory) {
  $scope.post = PikaPikaFactory[$stateParams.playlistId];
  $scope.playlists = PikaPikaFactory;

  $scope.openPDF = function() {
      console.log("/storage/sdcard1/Tagalog/PikaPika/" + $scope.post.pdfLink);
      $cordovaFileOpener2.open(
          "/storage/sdcard1/Tagalog/PikaPika/" + $scope.post.pdfLink, // Any system location, you CAN'T use your appliaction assets folder
          'application/pdf'
      ).then(function() {
          console.log('Success');
      }, function(err) {
          console.log('An error occurred: ' + JSON.stringify(err));
      });
  };

  $scope.openMp3 = function() {
      $cordovaFileOpener2.open(
          "/storage/sdcard1/Tagalog/PikaPika/" + $scope.post.mp3Link, // Any system location, you CAN'T use your appliaction assets folder
          'audio/mpeg'
      ).then(function() {
          console.log('Success');
      }, function(err) {
          console.log('An error occurred: ' + JSON.stringify(err));
      });
  };
})

.controller('PlaylistsCtrl', function($scope, $stateParams, CONSTANT, NewbieFactory, ElementaryFactory, PikaPikaFactory) {
  $scope.CONSTANT = CONSTANT;
  $scope.category = $stateParams.playlists;
  switch ($stateParams.playlists) {
    case CONSTANT.PIKA_PIKA:
      $scope.playlists = PikaPikaFactory;
      break;
    case CONSTANT.NEWBIE:
      $scope.playlists = NewbieFactory;
      break;
    case CONSTANT.ELEMENTARY:
      $scope.playlists = ElementaryFactory;
      break;
  }
  
})

.controller('PlaylistCtrl', function($scope, $stateParams, $cordovaFileOpener2, CONSTANT, NewbieFactory, ElementaryFactory, PikaPikaFactory) {
  $scope.CONSTANT = CONSTANT;
  $scope.category = $stateParams.playlists;
  switch ($stateParams.playlists) {
    case CONSTANT.PIKA_PIKA:
      $scope.playlists = PikaPikaFactory;
      break;
    case CONSTANT.NEWBIE:
      $scope.playlists = NewbieFactory;
      break;
    case CONSTANT.ELEMENTARY:
      $scope.playlists = ElementaryFactory;
      break;
  }
  
  $scope.post = $scope.playlists[$stateParams.playlistId];

  $scope.openPDF = function() {
      console.log("/storage/sdcard1/Tagalog/PikaPika/" + $scope.post.pdfLink);
      $cordovaFileOpener2.open(
          "/storage/sdcard1/Tagalog/PikaPika/" + $scope.post.pdfLink, // Any system location, you CAN'T use your appliaction assets folder
          'application/pdf'
      ).then(function() {
          console.log('Success');
      }, function(err) {
          console.log('An error occurred: ' + JSON.stringify(err));
      });
  };

  $scope.openMp3 = function() {
      $cordovaFileOpener2.open(
          "/storage/sdcard1/Tagalog/PikaPika/" + $scope.post.mp3Link, // Any system location, you CAN'T use your appliaction assets folder
          'audio/mpeg'
      ).then(function() {
          console.log('Success');
      }, function(err) {
          console.log('An error occurred: ' + JSON.stringify(err));
      });
  };
})
;
