App.factory('PikaPikaFactory', function PikaPikaFactory() {
    var posts = [{
        "id": 0,
        "title": "Pika-Pika – Anong Pangalan Mo?",
        "content": "We’ve all known each other for quite some time and fortunately we’ve all been courteous enough to introduce ourselves. But what if you encounter a stranger, a little lost kid, or a duwende perhaps? I’m sure they won’t be going all James Bond with their names. So in order to get the ball rolling in the friendship and familiarity department, here’s a quick lesson on asking someone’s name<br />",
        "mp3Link": "pika-pika-anong-pangalan-mo_.mp3",
        "pdfLink": null,
        "img": "pikapika/pika-pika-pangalan.jpg"
    }, {
        "id": 1,
        "title": "Pika Pika – Merry Christmas! Maligayang Pasko!",
        "content": "In Kalyespeak’s first EVER (and historic!) Pika-Pika, Cris and Mickey put their Santa hats on and dissect the phrase “Maligayang Pasko!”<br />",
        "mp3Link": "pika-pika-maligayang-pasko.mp3",
        "pdfLink": null,
        "img": null
    }, {
        "id": 2,
        "title": "Pika-pika! – Yiheeeeeee!",
        "content": "Ever notice that something romantic’s a-brewing between two of your friends? Ever see the subtle touches on the shoulder, the intimate whispers, the exceedingly awkward politeness? Ever want to put them on the spot?! Well, in this Pika-pika, you’ll learn how! With just one annoying word (can it even be considered a word?!), you’ll be putting romantic issues of others out in the open (or even make romantic issues out of nothing!). So, listen in and master the art of the perfectly-timed ‘Yihee!’.<br />",
        "mp3Link": "pika-pika-yihee.mp3",
        "pdfLink": null,
        "img": "pikapika/pika-pika-yihee.jpg"
    }, {
        "id": 3,
        "title": "Pika Pika! – Ang Bait Mo Naman!",
        "content": "Why, thank you, you’re so kind! Ever the polite language, Filipino can actually be used sarcastically, as is often the case with this phrase, “ang bait mo naman!” Listen in as Cris, Mickey and Jovitt discuss this ubiquitous phrase.<br /><br />",
        "mp3Link": "pika-pika-ang-bait-mo-naman.mp3",
        "pdfLink": null,
        "img": "pikapika/pika-pika-bait.jpg"
    }, {
        "id": 4,
        "title": "Pika Pika – Inuman na! Let’s all get drunk!",
        "content": "Get the party started with this new phrase! Learn how Filipinos officially declare the festivities open. Are you ready? Inuman na!<br />",
        "mp3Link": "pika-pika-inuman-na.mp3",
        "pdfLink": null,
        "img": null
    }, {
        "id": 5,
        "title": "Pika-pika! – Bahala Na Si Batman",
        "content": "Caught in a pickle? Bite off more than you can chew? Crap hit the fan? Thinking of eating that balut? In love with Katy Perry but you’re not Russell Brand? Well, if you don’t know what to do, then listen in!  Cris and Mickey talk about this odd, yet interesting, Filipino expression!<br />",
        "mp3Link": "pika-pika-bahala-na-si-batman.mp3",
        "pdfLink": null,
        "img": null
    }, {
        "id": 6,
        "title": "Pika Pika! – Ang Bait Mo Naman!",
        "content": "Why, thank you, you’re so kind! Ever the polite language, Filipino can actually be used sarcastically, as is often the case with this phrase, “ang bait mo naman!” Listen in as Cris, Mickey and Jovitt discuss this ubiquitous phrase.<br /><br />",
        "mp3Link": "pika-pika-ang-bait-mo-naman.mp3",
        "pdfLink": null,
        "img": "pikapika/pika-pika-bait.jpg"
    }, {
        "id": 7,
        "title": "Extra Ispesyal Pika-Pika! – Lasing Ako! (With Special Guest Beth!)",
        "content": "We know, we know,  Pika-pika’s usually a once-a-month thing… but not this month – especially with this EXTRA SPECIAL PIKA-PIKA! Listen in as Kalyespeak finally tackles the “L” word and Cris talks about his upcoming foray in Beijing! And if that’s not enough, we have our very first guest – all the way from New York! Ispesyal na ispesyal! :)<br />",
        "mp3Link": "extra-ispesyal-pika-pika-lasing-ako-with-beth-and-a-bunch-of-other-people.mp3",
        "pdfLink": null,
        "img": "pikapika/ei-pika-pika-lasing.jpg"
    }, {
        "id": 8,
        "title": "Pika Pika! – Merienda Tayo!",
        "content": "Need a break? Have a banana cue! Filipinos love to eat so much that in-between meals are part of our culinary culture. What do we call it? Merienda! In this lesson, we’re giving you another excuse to go out and have a break – because we know you can never have too much of that. Learn this short phrase and you’ll be able to invite a friend to come along.<br />",
        "mp3Link": "pika-pika-merienda-tayo.mp3",
        "pdfLink": null,
        "img": "pikapika/pika-pika-merienda.jpg"
    }, {
        "id": 9,
        "title": "Pika-pika! – Yiheeeeeee!",
        "content": "Ever notice that something romantic’s a-brewing between two of your friends? Ever see the subtle touches on the shoulder, the intimate whispers, the exceedingly awkward politeness? Ever want to put them on the spot?! Well, in this Pika-pika, you’ll learn how! With just one annoying word (can it even be considered a word?!), you’ll be putting romantic issues of others out in the open (or even make romantic issues out of nothing!). So, listen in and master the art of the perfectly-timed ‘Yihee!’.<br />",
        "mp3Link": "pika-pika-yihee.mp3",
        "pdfLink": null,
        "img": "pikapika/pika-pika-yihee.jpg"
    }, {
        "id": 10,
        "title": "Pika Pika! – Merienda Tayo!",
        "content": "Need a break? Have a banana cue! Filipinos love to eat so much that in-between meals are part of our culinary culture. What do we call it? Merienda! In this lesson, we’re giving you another excuse to go out and have a break – because we know you can never have too much of that. Learn this short phrase and you’ll be able to invite a friend to come along.<br />",
        "mp3Link": "pika-pika-merienda-tayo.mp3",
        "pdfLink": null,
        "img": "pikapika/pika-pika-merienda.jpg"
    }, {
        "id": 11,
        "title": "Pika-Pika – Anong Pangalan Mo?",
        "content": "We’ve all known each other for quite some time and fortunately we’ve all been courteous enough to introduce ourselves. But what if you encounter a stranger, a little lost kid, or a duwende perhaps? I’m sure they won’t be going all James Bond with their names. So in order to get the ball rolling in the friendship and familiarity department, here’s a quick lesson on asking someone’s name<br />",
        "mp3Link": "pika-pika-anong-pangalan-mo_.mp3",
        "pdfLink": null,
        "img": "pikapika/pika-pika-pangalan.jpg"
    }, {
        "id": 12,
        "title": "Pika Pika – Happy New Year! Manigong Bagong Taon!",
        "content": "Get ready for some fireworks as we give you a lil quicky on how to welcome the New Year — KALYESPEAK STYLE! Manigong Bagong Taon!<br />",
        "mp3Link": "pika-pika-manigong-bagong-taon.mp3",
        "pdfLink": null,
        "img": null
    }, {
        "id": 13,
        "title": "Pika-Pika! – Susmaryosep!",
        "content": "Ever arrive home and find your kid doing something he isn’t supposed to be doing? Ever had a bad day at work, get stuck 5 hours in ridiculous traffic, and get a text message that your boss wants you to come back for overtime? Ever been flabbergasted and wanting for a Filipino expletive to express yourself? Well, here it is! But unlike other languages, it’s not a dirty word – in fact, it’s a call to the high heavens. Listen in as the Kspeak trio invoke the family of all families in this peculiar Filipino expression!<br />",
        "mp3Link": "pika-pika-susmaryosep.mp3",
        "pdfLink": null,
        "img": "pikapika/pika-pika-susmaryosep.jpg"
    }, {
        "id": 14,
        "title": "Extra Ispesyal Pika-Pika! – Lasing Ako! (With Special Guest Beth!)",
        "content": "We know, we know,  Pika-pika’s usually a once-a-month thing… but not this month – especially with this EXTRA SPECIAL PIKA-PIKA! Listen in as Kalyespeak finally tackles the “L” word and Cris talks about his upcoming foray in Beijing! And if that’s not enough, we have our very first guest – all the way from New York! Ispesyal na ispesyal! :)<br />",
        "mp3Link": "extra-ispesyal-pika-pika-lasing-ako-with-beth-and-a-bunch-of-other-people.mp3",
        "pdfLink": null,
        "img": "pikapika/ei-pika-pika-lasing.jpg"
    }, {
        "id": 15,
        "title": "Pika Pika! – Pahingi!",
        "content": "Imagine yourself in the desert, the malevolent sun beating down on your burning skin, your throat as dry as the sand around you. Then, out of nowhere, you see an angel holding a bottle of Coca-cola. The thing is, the angel only speaks Filipino. So what now? Well, listen in as Cris and Mickey discuss the magic word ‘Pahingi’ in this edition of Pika-pika!<br /><br />",
        "mp3Link": "pika-pika-pahingi.mp3",
        "pdfLink": null,
        "img": "pikapika/pikapikapahingi.jpeg"
    }, {
        "id": 16,
        "title": "Pika-pika! – Bahala Na Si Batman",
        "content": "Caught in a pickle? Bite off more than you can chew? Crap hit the fan? Thinking of eating that balut? In love with Katy Perry but you’re not Russell Brand? Well, if you don’t know what to do, then listen in!  Cris and Mickey talk about this odd, yet interesting, Filipino expression!<br />",
        "mp3Link": "pika-pika-bahala-na-si-batman.mp3",
        "pdfLink": null,
        "img": null
    }, {
        "id": 17,
        "title": "Pika-Pika! – Susmaryosep!",
        "content": "Ever arrive home and find your kid doing something he isn’t supposed to be doing? Ever had a bad day at work, get stuck 5 hours in ridiculous traffic, and get a text message that your boss wants you to come back for overtime? Ever been flabbergasted and wanting for a Filipino expletive to express yourself? Well, here it is! But unlike other languages, it’s not a dirty word – in fact, it’s a call to the high heavens. Listen in as the Kspeak trio invoke the family of all families in this peculiar Filipino expression!<br />",
        "mp3Link": "pika-pika-susmaryosep.mp3",
        "pdfLink": null,
        "img": "pikapika/pika-pika-susmaryosep.jpg"
    }];
    return posts;
});
