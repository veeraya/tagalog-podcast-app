App.factory('NewbieFactory', function NewbieFactory() {
	var posts = [
  {
    "id": 0,
    "title": "Newbs - Let's Play! Duwende Ka!",
    "content": "That little man isn't real you know. With his little bowler hat, long beard, crooked nose, and the fact that he's not more than a foot tall, it's pretty obvious. That little man is a duwende! Keep your kids from playing with these little fellas! Cris and Mickey teach you what to do when encountering a dwarf, and Jovitt adds his own insights on Philippine mythological creatures.<br /><blockquote>A: Bata, maglaro tayo!<br />B: Uuuh, sino ka?<br />A: Kaibigan mo ako!<br />B: Teka, duwende ka!<div class='section-heading'>Translation:</div>A: Hey kid, let's play!<br />B: Umm, who are you?<br />A: I'm your friend!<br />B: Wait, you're a dwarf!</blockquote>",
    "pdfLink": "newbs-lets_play_duwende_ka.pdf",
    "img" : null,
    "mp3Link": "newbs-lets-play-duwende-ka.mp3"
  },
  {
    "id": 1,
    "title": "Newbs - Street Food Series: Taho!",
    "content": "Is it a drink or a snack? When it comes to taho, who cares?! It's cheap and good, that's all that matters! In this lesson from the Street Food Series, Mickey and Jov talk about the hot and sweet concoction that is taho. Raz and Paulo chime in and explain what it's made of.<br /><blockquote>A: Taho! Taho!<br />B: Manong, magkano po isang baso?<br />A: P10 yung maliit. P15 yung malaki.<br />B: Isang maliit po. Padamihan po ng sago.<br /> <div class='section-heading'>Translation:</div> A: Taho! Taho!<br />B: Sir, how much is a glass?<br />A: P10 for the small one, P15 for the larger one.<br />B: One small one please. And please put a lot of sago.</blockquote>",
    "pdfLink": "newbs-street_food_series_taho.pdf",
    "img" : "newbies/taho.jpg",
    "mp3Link": "newbs-street_food_taho.mp3"
  },
  {
    "id": 2,
    "title": "Newbs - Do You have plans tomorrow? Nood tayo ng sine.",
    "content": "Want to ask that cute girl out, but don't quite know how to phrase it in Filipino? Well, here's the lesson for you. Cris and Mickey will teach you how to ask if that pretty morena Pinay has plans for tomorrow. Flash that killer smile along with that perfect Pinoy accent and you may just find yourself scoring a date. The vulture Jovitt joins the duo and gives his observations about first dates.<br /><blockquote>A: Sharon, may plano ka ba bukas?<br />B: Wala naman.<br />A: Nood tayo ng sine.<br />B: Sige. Kita-kits.<br /><div class='section-heading'>Translation:</div>A: Sharon, do you have plans for tomorrow?<br />B: Nope, don't have any.<br />A: Let's watch a movie.<br />B: Sure, see you.</blockquote>",
    "pdfLink": null,
    "img" : "newbies/plano.jpg",
    "mp3Link": "newbs-plans_-nood-tayo-ng-sine.mp3"
  },
  {
    "id": 3,
    "title": "Newbs - Asking For A Picture. Isa, Dalawa, Tatlo!",
    "content": "Traveling around the Philippines alone and dying to have your picture taken with that beautiful Pinay behind you? Don't fret! In this lesson, Cris and Mickey will not only teach you how to charm the locals with your exquisite Filipino, but you'll also learn how to ask someone to take your picture for you! Jovitt gives his two cents on the famous Philippine sunset and where best to see it!<br /><blockquote>A: Kuya, pwede papicture?<br />B: Sige, anong background gusto mo?<br />A: Umm. kasama yung sunset.<br />B: Okey! Isa.. dalawa.. tatlo! Smile!<br /><div class='section-heading'>Translation:</div>A: Kuya, can you take my picture?<br />B: Sure, what background do you want?<br />A: Umm, with the sunset.<br />B: Okay! One, two, three! Smile!</blockquote><br />Want to learn how to request for more stuff? Check out the PDF!",
    "pdfLink": "newbs-asking_for_a_picture_isa_dalawa_tatlo.pdf",
    "img" : "newbies/42-16679734.jpg",
    "mp3Link": "newbs-asking-for-a-picture-isa-d.mp3"
  },
  {
    "id": 4,
    "title": "Newbs - It's broken! Pwede ipapalit?",
    "content": "Ever buy your dream TV, bring it home, set it up in your living room, and just when you're ready to kick-back into LCD-heaven, you find out it doesn't work? If you have, then here's how to say it in Filipino - the simplest way possible. Join Mickey and Cris as they explore the world of brokenness and um, fixed-ness.<br /><blockquote>A: Kuya, pwede ipapalit?<br />B: Bakit?<br />A: Sira siya eh.<br />B: Hmm. ayos siya. Hindi lang naka-plug<br /><div class='section-heading'>Translation:</div>A: Kuya, can we have this exchanged?<br />B: Why?<br />A: It's broken.<br />B: Hmm, it's working fine. It just wasn't plugged.</blockquote>",
    "pdfLink": null,
    "img" : "newbies/sira2.jpg",
    "mp3Link": "newbs-its-broken-pwede-ipapalit.mp3"
  },
  {
    "id": 5,
    "title": "Newbs - Take Out! Pabalot!",
    "content": "Eating out and realized that you ordered too much? That leftover lechon sure is looking good for breakfast tomorrow. But what to do, what to say? Well, in this month's Kspeak lesson, Cris and Mickey will teach you how to say 'take out!' faster than you can say 'wow, sarap!' Razi, Bernice, and Jovitt also talk about 'takaw mata' in the culture portion.<br /><blockquote>A: Ang dami pang tira!<br />B: Pabalot na lang kaya natin?<br />A: Sige, sabihin mo sa ate!<br />B: Ate, pabalot!<br /><div class='section-heading'>Translation:</div>A: There's so much leftovers!<br />B: You think we should take it home?<br />A: Yeah!! Tell the waitress!<br />B:  Miss! Please have this wrapped for take out!</blockquote>",
    "pdfLink": "newbs-take-out-pabalot.pdf",
    "img" : "newbies/pabalot.jpg",
    "mp3Link": "newbs-take-out-pabalot1.mp3"
  },
  {
    "id": 6,
    "title": "Newbs-Thief! Saklolo!",
    "content": "You're walking down the street, and next thing you know it, a guy comes running from behind you, snatches your wallet and runs off to the horizon. What are you to do? Shout 'Saklolo!' of course! Listen in as the Kspeak Crew talks about this perilously termed Filipino word!<br /><blockquote>A: Saklolo! Magnanakaw! Saklolo!<br />B: Saan siya pumunta?!<br />A: Doon banda!<br />B: Ako bahala!<br /><div class='section-heading'>Translation:</div>A: Help! Thief! Help!<br />B: Where'd he go?<br />A: Over there!<br />B: I'll handle it!</blockquote>",
    "pdfLink": "newbie-thief_saklolo.pdf",
    "img" : "newbies/saklolo.jpg",
    "mp3Link": "newbs-thief-saklolo.mp3"
  },
  {
    "id": 7,
    "title": "Newbs - I Love You! Pakita Mo .",
    "content": "Ah, love. The four letter word that starts a life of happiness and fulfillment. The four letter word that makes the world go round. The four letter word that makes us gush and see little bunny rabbits hop around with unicorns prancing on colorful rainbows. Um, ok, maybe not the last part. Sadly, it's also the four letter word that causes awfully awkward conversations. such as this one. Listen and learn to profess your love in Filipino with Mickey and Cris.<br /><blockquote>A: Pare, alam mo ba? Mahal kita.<br />B: Huwag mo sabihin iyan. Pakita mo na lang.<br /><div class='section-heading'>Translation:</div>A: Dude, did you know? I love you.<br />B: Don't say that. just show it.</blockquote>",
    "pdfLink": null,
    "img" : "newbies/pakitamo.jpg",
    "mp3Link": "newbs-i-love-you-pakita-mo.mp3"
  },
  {
    "id": 8,
    "title": "Newbs-My Boil! Tara Na Sa Ospital!",
    "content": "You'll know an emergency when you see one - and a festering boil ready to pop is one of them. Listen in as Jovitt and Mickey tackle an emergency in this episode of Kalyespeak. Also, Razi is making his debut as the newest member of the team as the ever-knowledgable Society Snail. This promises to be an insightful lesson of boils, emergencies, useful language, and circumcision (yes, you read it right, circumcision.)<br /><blockquote>A: Sasabog na yung pigsa ko!<br />B: Tara na sa ospital!<br />A: Bilis ang sakit!<br /><div class='section-heading'>Translation:</div>A: My boil is going to explode!<br />B: Let's go to the hospital!<br />A: Hurry! It hurts!!!</blockquote>",
    "pdfLink": "newbs-my_boil_tara_na_sa_ospital.pdf",
    "img" : "newbies/ospital.jpg",
    "mp3Link": "newbs-my_boil_tara_na_sa_ospital.mp3"
  },
  {
    "id": 9,
    "title": "Newbs - DVD! Dibidi!",
    "content": "That little man whispering to your ear Dibidi, Eks, eks, buld, buld might creep you out, but he's actually your friendly DVD bootlegger - source of your pirated DVD needs, whether it be the latest Oscar-award winning film or the skankiest lewd movie. Learn how to haggle with the man with this lesson with Cris and Mickey, and listen to Jov as he gives his insights on the dangers of buying those darn discs.<br /><blockquote>A: Dibidi, dibidi! One-fifty, isa!<br />B: Ang mahal naman! Wala bang presyong kaibigan?<br />A: Sixty, last price.<br />B: Ayos! Pabili sampu!<br /><div class='section-heading'>Translation:</div>A: DVD, DVD! One hundred fifty pesos for one!<br />B: How expensive!, Don't you have a discounted price?<br />A: Sixty, last price.<br />B: Ok! I'll buy ten!</blockquote><br /> Dying to say 'How cheap!' or 'That's so long!?' Check out the PDF!",
    "pdfLink": null,
    "img" : null,
    "mp3Link": "newbs-dvd-dibidi.mp3"
  },
  {
    "id": 10,
    "title": "Newbs - Yaya, I'm Scared! Mumu!",
    "content": "We go back to the days of yore, where things were simpler, and all our questions were answered by our yayas. Yayas?! Yep, in this episode, Cris and Mickey discuss the Filipino cultural phenomenon called 'yayas.' We're not talking about no creepy sisterhood here, but nannies!<br /><blockquote>A: Joeyboy! Huwag kang pupunta diyan!<br />B: Bakit, yaya?<br />A: May mumu diyan!<br />B: Takot ako. huhuhu.<br /><div class='section-heading'>Translation:</div>A: Joeyboy! Don't go there!<br />B: Why, yaya?<br />A: There's a ghost there!!<br />B: I'm scared! huhuhu.</blockquote>",
    "pdfLink": "newbs-yaya_im_scared.pdf",
    "img" : "newbies/mumu.jpeg",
    "mp3Link": "newbs-yaya-im-scared3.mp3"
  },
  {
    "id": 11,
    "title": "Newbs - Checking the Taxi Meter. Gumagana.",
    "content": "Getting around Manila is easy, but getting around without being cheated by the taxi driver is another matter. In this long-overdue Kalyespeak lesson (sorry for the delay guys!), we'll learn from Mickey and Jovitt how to check if the taxi meter is actually working! Razi and Paulo join in and share their tips on taking a taxi around Manila. <blockquote> A: Bos, gumagana ba metro mo?<br /> B: Oo, bakit?<br /> A: Ang mahal kasi eh.<br /> B: Tama yan, traffic eh.<br /> <div class='section-heading'>Translation:</div> A: Sir, is your taxi meter working?<br /> B: Yes, why?<br /> A: It's so expensive.<br /> B: That's correct, it's just really traffic. </blockquote>",
    "pdfLink": "newbs-checking_the_taxi_meter-gumagana.pdf",
    "img" : null,
    "mp3Link": "newbs-checking-the-taxi-meter-gumagana.mp3"
  },
  {
    "id": 12,
    "title": "Newbs - I Lost My Wallet! Putek!",
    "content": "You get home one day, excited to show your friend your autograph of Anne Curtis. And then you realize you lost your wallet on the way! Crud! How do you express that in Filipino? Well, listen in as Cris and Mickey teach you a mild swear word, and talk about Anne Curtis.<br /><blockquote>A: Putek! Nawawala wallet ko!<br />B: Ano laman?<br />A:P10,000! At yung autograph ni Anne Curtis!<br />B: Diyos ko!<br /><div class='section-heading'>Translation:</div>A: Crud! I lost my wallet!<br />B: What was in it?<br />A: P10,000! And my autograph of Anne Curtis!<br />B: My God!</blockquote>",
    "pdfLink": null,
    "img" : "newbies/nawawala.jpg",
    "mp3Link": "newbs_i-lost-my-wallet-putek.mp3"
  },
  {
    "id": 13,
    "title": "Newbs - The Curious Case of the Filipino Greeting. Kumain Ka Na Ba?",
    "content": "So, we've learned how to say 'kamusta' (or 'kumusta' - whichever way you want to spell it), but is there a more Filipino way of greeting people? IS THERE?! Well, indeed there is! Listen in as Cris and Mickey break down this lesson and Jovitt comes in with his four cents on this peculiar Filipino greeting.<br /><blockquote>A: Oh, hijo. Kumusta ang biyahe?<br />B: Ok naman po.<br />A: Kumain ka na ba?<br />B: Opo.<br /><div class='section-heading'>Translation:</div>A: Oh, son. How was your trip?<br />B: It was ok.<br />A: Have you eaten?<br />B: Yes sir.</blockquote>",
    "pdfLink": null,
    "img" : "newbies/kumain1.jpg",
    "mp3Link": "newbs-the-curious-case-of-the-filipino-greeting-kumain-ka-na-ba_.mp3"
  },
  {
    "id": 14,
    "title": "Newbs - Is It Far? Kaunti Na Lang.",
    "content": "Ever had that trip that seemed like forever? Well, we're pretty sure it won't compare to the trip the father and son in this episode are having. hey, there's no longer road than the road in a post-apocalyptic Philippines. Hold the tissue if you have a daddy issue, and listen in as Cris and Mickey brush off the soot and teach you helpful Filipino. Jov keeps on walking with a note for pedestrians out there.<br /><blockquote>A: Malayo pa ba, itay?<br />B: Malapit na anak.<br />A: Pagod na ako.<br />B: Kaunti na lang.<br /><div class='section-heading'>Translation:</div>A: Is it still far, dad?<br />B: It's near already.<br />A: I'm already tired.<br />B: Just a little more.</blockquote>",
    "pdfLink": "newbs-is_it_far_kaunti_na_lang.pdf",
    "img" : null,
    "mp3Link": "newbs-is-it-far_-kaunti-na-lang.mp3"
  },
  {
    "id": 15,
    "title": "Newbs - Is She In? Anong Oras Siya Babalik?",
    "content": "Ever gone to the house of the girl of your dreams. and realize she's gone? Ever wonder when she's coming back? Ever realize her name was Juliebeth? Well, if you said yes to the first two questions, this is the lesson for you. And if you're wondering if Juliebeth is a real name, then this is ALSO the lesson for you. Join Cris, Mickey and Jov in this lesson and find out when Juliebeth is coming back!<br /><blockquote>A: Andiyan po ba si Juliebeth?<br />B: Ay. wala po.<br />A: Anong oras po siya babalik?<br />B: Hindi ko po alam. Baka bukas pa.<br /><div class='section-heading'>Translation:</div>A: Is Juliebeth in?<br />B: Oh.. she's not here.<br />A: What time will she be back?<br />B: I don't know. Probably tomorrow.<br /></blockquote><br /> Learn more about Juliebeth here!",
    "pdfLink": "newbs-is_she_in_anong_oras_babalik.pdf",
    "img" : null,
    "mp3Link": "newbs-is-she-in_-anong-oras-babalik_.mp3"
  },
  {
    "id": 16,
    "title": "Newbs - You're Dead! Umuuulan!",
    "content": "We only have two seasons here in the Philippines - the hot and the wet. Ok, that just sounded dirty, but you've got the point. And right now, we're smack in the middle of the wet rainy season! So don't forget you're umbrellas.. you never know when the rain's going to pour! Cris and Mickey talk about rain, umbrellas and death, as Jov chimes in with his cultural insights.<br /><blockquote>A: Umuulan! Umuulan!<br />B: Hala! Wala akong payong!<br />A: Patay ka! Umuulan! Umuulan!<br />B: Oo na! Oo na! Patay ako! PATAY AKO!<br /><div class='section-heading'>Translation:</div>A: It's raining! It's raining!<br />B: Oh no! I don't have an umbrella!<br />A: You're dead! It's raining! It's raining!<br />B: Yeah! Yeah! I'm dead! I'm dead!</blockquote><br />Get drenched with more knowledge with the supplemental PDFs here!",
    "pdfLink": "newbs-umuulan_umuulan.pdf",
    "img" : null,
    "mp3Link": "newbs-you_re-dead-umuulan.mp3"
  },
  {
    "id": 17,
    "title": "Newbs - Christmas episode! Ano plano natin?",
    "content": "Christmas, ah, that lovely holiday. Although Christmas in the Philippines is different from the Western idea of Christmas - no snowmen or jingle bells here, Pinoys have their own special way of bringing in the cheer. For culture talk, Mickey and Cris discuss different Filipino traditions and share a special Christmas message for all the Kalyespeak supporters.  Merry Christmas everyone! <br /><blockquote>A: Nay, ano plano natin sa Pasko?<br />B: Simbang gabi tapos noche Buena.<br />A: Kailan tayo magbubukas ng regalo?<br />B: Pagkatapos ng noche Buena.<br /><div class='section-heading'>Translation:</div>A: Mom, what are we doing for Christmas?<br />B: Midnight mass, then Christmas dinner.<br />A: When will we open gifts?<br />B: After the Christmas dinner.</blockquote>",
    "pdfLink": "newbs-christmas_episode_ano_plano_natin.pdf",
    "img" : null,
    "mp3Link": "newbs-christmas-episode-ano-plano-natin_.mp3"
  },
  {
    "id": 18,
    "title": "Newbie - I'm Thirsty. Ito, Tubig",
    "content": "Your throat is dry. Your lips are cracked. You're parched, admit it. There's no shame in saying you're thirsty. And because we have no shame here in Kalyespeak, we'll be teaching you how to say 'I'm thirsty' in Filipino (talk about a segue!). Tune in as Mickey and Jovitt talk about thirst and water. Listen in closely and you might just guess where Cris G has gone off to!<br /><blockquote>A: Nauuhaw ako.<br />B: Oh, ito tubig.<br />A: Salamat po.<br /><div class='section-heading'>Translation:</div>A: I'm thirsty.<br />B: Here, have some water.<br />A: Thanks.</blockquote>",
    "pdfLink": "newbie-im_thirsty_ito_tubig.pdf",
    "img" : "newbies/tubig.jpg",
    "mp3Link": "newbie-im_thirsty_ito_tubig1.mp3"
  },
  {
    "id": 19,
    "title": "Newbs - Are you ready? Teka lang.",
    "content": "Aiyo! So you're friend is one of those slow types - 30 minutes to get ready to take a shower, another 30 in the shower, and yet another 30 getting out of the shower. Cris and Mickey give you a lesson on how to ask if he's ready (which he probably isn't). Jovitt flies in to talk about 'Filipino time.'<br /><blockquote>A: Buboy, handa ka na ba?<br />B: Hindi pa.<br />A: Tara na! Late na tayo!<br />B: Teka lang!<br /><div class='section-heading'>Translation:</div>A: Buboy, are you ready?<br />B: Not yet.<br />A: Come on! We're late already!<br />B: Hold up a bit!</blockquote>",
    "pdfLink": null,
    "img" : null,
    "mp3Link": "newbs-are-you-ready_-teka-lang.mp3"
  },
  {
    "id": 20,
    "title": "Newbs-WIFI? Libre!",
    "content": "Ever go to a coffee shop and feel the need to go online (to listen to the newest episode of kalyespeak, yeh?)?! Well, here's just the lesson for you! Whether you call it wifi or wee-fee, we all need it, so here's how to say it! Join Mickey, Jovitt and Razi as they talk about the language, free wifi and coffee shops<br /> <blockquote> A: May WIFI ba dito?<br /> B: Siyempre.<br /> A: Ano yung password?<br /> B: Hindi kailangan. Libre eh.<br /> <div class='section-heading'>Translation:</div> A: Is there WIFI here?<br /> B: Of course!<br />A: What's the password?<br /> B: You don't need it because it's free! </blockquote>",
    "pdfLink": "newbie-wifi_libre.pdf",
    "img" : "newbies/wifi-copy.jpg",
    "mp3Link": "newbs-wifi_libre.mp3"
  },
  {
    "id": 21,
    "title": "Newbs-Friendship at its Finest",
    "content": "Ever had those days when you just felt downright ugly? Yeah? Then this lesson is for you! Learn how to be honest with yourself with this lesson hosted by Cris and Mickey! The intricacies and nuances of the Filipino culture is discussed by Bernice, Razi, and Jovitt!<br /><blockquote>A: Ang panget ko talaga.<br />B: Hindi ka panget<br />A: Salamat. Ang bait mo talaga.<br />B: Hindi ka lang gwapo.<br /><div class='section-heading'>Translation:</div>A: I'm so ugly!<br />B: You're not ugly.<br />A: Thanks, you're so kind.<br />B: You're just not handsome.</blockquote>",
    "pdfLink": "newbs-friendship_at_its_finest.pdf",
    "img" : "newbies/gwapo.jpg",
    "mp3Link": "newbs-friendship_at_its_finest.mp3"
  },
  {
    "id": 22,
    "title": "Newbs - I'm from the States. Astig.",
    "content": "Just because you're white doesn't mean you're from the States. We all know that's true, but give 40 years of colonization plus another 60 years of neo-colonization and you have Filipinos thinking all white folks come from the land of the free. Well, in this lesson, Cris and Mickey teach you how to clarify where you are from, Cris also adds some flavor with his etymology of the word 'astig'. Jovitt discusses the importance of hometowns and provinces.<br /><blockquote>A: Excuse me po, taga-rito ka ba?<br />B: Hindi, taga-Amerika ako.<br />A: Talaga? Taga-Canada ako.<br />B: Astig.<br /><div class='section-heading'>Translation:</div>A: Excuse me, are you from here?<br />B: No, I'm from the States.<br />A: Really? I'm from Canada.<br />B: Cool.</blockquote>",
    "pdfLink": "newbs-im_from_the_states_astig.pdf",
    "img" : "newbies/amerika.jpg",
    "mp3Link": "newbs-im-from-the-states-astig.mp3"
  },
  {
    "id": 23,
    "title": "Newbs - Does It Go? Iyakin.",
    "content": "That high-pitched androgynous being that is Zsazsa is back, and now she's shopping. Of course, his/her friend is back as well, with a new mean putdown in store for poor Zsazsa. Cris and Mickey explain the language of colors, matching clothes, and Christmas trees (go figure!).<br /><blockquote>A: Bagay ba?<br />B: Pulang t-shirt, green na pantalon. Ano ka - Krismas tree?<br />A: Ang sama mo.<br />B: Iyakin.<br /><div class='section-heading'>Translation:</div>A: Does it go?<br />B: Red shirt, green pants. What do you think you are - a Christmas tree?<br />A: You're so mean.<br />B: Crybaby.</blockquote>",
    "pdfLink": "newbs-does_it_go_iyakin.pdf",
    "img" : null,
    "mp3Link": "newbs-does-it-go_-iyakin.mp3"
  },
  {
    "id": 24,
    "title": "Newbs - It's so Hot! Arte mo!",
    "content": "The vicious heat of the Filipino summer getting to you? It's not all about the tropical paradise of beaches, bikini-clad Pinays, and cold beer you know. For the rest of us who aren't at the beach, we have to endure scorching 37 degree temperatures, which can lead to 'inis init'. Jovitt discusses this tropical phenomenon while Cris and Mickey take a looksy at a  dialogue which can go 'either way.'<br /><blockquote>A: Ang init ngayon! Natutunaw ako!<br />B: Exage ka naman! Hindi kaya.<br />A: Bakit ka ganyan?<br />B: Ang arte mo eh.<br /><div class='section-heading'>Translation:</div>A: It's so hot today! I'm melting!<br />B: You're exaggerating! It's not THAT hot.<br />A: Why do you have to be that way? :(<br />B: Because you're so fussy.</blockquote>",
    "pdfLink": null,
    "img" : "newbies/init.jpg",
    "mp3Link": "newbs-its-so-hot-arte-mo.mp3"
  },
  {
    "id": 25,
    "title": "Newbs - Adobo. AGAIN?!",
    "content": "Ever got home craving for a double cheese burger and all you got was a crummy salad? Ever happen to you twice. thrice. quatrice (there such a word?)??? Well, in this lesson, Cris and Mickey (who both sport a weird English accent) will teach you how to show disappointment, albeit in a rude way. Jovitt pipes in with his recipe for the delectable Filipino dish - adobo.<br /><blockquote>A: Ano ulam natin?<br />B: Adobo po.<br />A: Adobo na naman??? Sawa na ako sa adobo!!!<br /><div class='section-heading'>Translation:</div>A: What food are we having?<br />B: Adobo.<br />A: Adobo AGAIN??? I'm sick and tired of adobo!!!</blockquote>",
    "pdfLink": "newbs-adobo_again.pdf",
    "img" : null,
    "mp3Link": "newbs-adobo-again_.mp3"
  },
  {
    "id": 26,
    "title": "Newbs - Street Food Series: This is Delicious! Balut!",
    "content": "People say Filipinos eat anything. And, honestly, maybe we do. In the pilot lesson of the new Street Food Series, the Kalyespeak crew will peel away the shell of everyone's favorite egg - Balut. What is it? Is it good? Is that a feather?! Find out the answers here!<br /><blockquote>A: Ang sarap nito!<br />B: Ano ba iyan?<br />A: Balut!<br />B: Wow! Pahingi!<br /><div class='section-heading'>Translation:</div>A: This is delicious!<br />B: What is that?<br />A: Balut!<br />B: Wow! Give me some!</blockquote>",
    "pdfLink": "newbs-street_food_series_this_is_delicious_balut.pdf",
    "img" : "newbies/balut.jpg",
    "mp3Link": "newbs-street_food_series_this_is_delicious_balut.mp3"
  },
  {
    "id": 27,
    "title": "Newbs - I'll Go Ahead. Ingat!",
    "content": "We say goodbye to you today. okay okay, don't fret. We're TEACHING you how to say goodbye today. In this short but sweet lesson, Cris and Mickey will teach you some ways to say goodbye to a friend or make a smooth getaway from an annoying person. Jovitt hops on the culture trail and talks about the Filipino text messaging culture.<br /><blockquote>A: Una na ako pare!<br />B: Sige, text-text na lang!<br />A: Okey, kita-kits!<br />B: Ingat!<br /><div class='section-heading'>Translation:</div>A: I'll go ahead mate.<br />B: Sure, just text me!<br />A: Okay, see you!<br />B: Take care!</blockquote>",
    "pdfLink": "newbs-ill_go_ahead_ingat.pdf",
    "img" : "newbies/kita-copy1.jpg",
    "mp3Link": "newbs-ill-go-ahead-ingat.mp3"
  }
];
    
    return posts;
});