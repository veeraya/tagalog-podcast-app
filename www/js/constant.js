App.factory('CONSTANT', function CONSTANT() {
	var CONSTANT = {
		BASE_PLAYLIST_URL : "#/app/playlists/",
		PIKA_PIKA : "Pika-Pika",
		PIKA_PIKA_FILE_PATH : "/storage/sdcard1/Tagalog/PikaPika/",
		NEWBIE : "Newbie",
		NEWBIE_FILE_PATH : "/storage/sdcard1/Tagalog/Newbies/",
		ELEMENTARY : "Elementary",
		ELEMENTARY_FILE_PATH : "/storage/sdcard1/Tagalog/Elementary/"
	};
	return CONSTANT;
});