[
  {
    "id": 0,
    "title": "Newbs - Let's Play! Duwende Ka!",
    "content": "That little man isn't real you know. With his little bowler hat, long beard, crooked nose, and the fact that he's not more than a foot tall, it's pretty obvious. That little man is a duwende! Keep your kids from playing with these little fellas! Cris and Mickey teach you what to do when encountering a dwarf, and Jovitt adds his own insights on Philippine mythological creatures.\nA: Bata, maglaro tayo!\n\nB: Uuuh, sino ka?\n\nA: Kaibigan mo ako!\n\nB: Teka, duwende ka!\nTranslation:\nA: Hey kid, let's play!\n\nB: Umm, who are you?\n\nA: I'm your friend!\n\nB: Wait, you're a dwarf!\n\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-lets-play-duwende-ka.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload\nWanna play some more? Download our supplementary PDF!\nMusic: �Uh Oh� by Taken by Cars\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebook2MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-lets_play_duwende_ka.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-lets-play-duwende-ka.mp3"
  },
  {
    "id": 1,
    "title": "Newbs - Street Food Series: Taho!",
    "content": "Is it a drink or a snack? When it comes to taho, who cares?! It's cheap and good, that's all that matters! In this lesson from the Street Food Series, Mickey and Jov talk about the hot and sweet concoction that is taho. Raz and Paulo chime in and explain what it's made of.\nA: Taho! Taho!\n\nB: Manong, magkano po isang baso?\n\nA: P10 yung maliit. P15 yung malaki.\n\nB: Isang maliit po. Padamihan po ng sago.\nA: Taho! Taho!\n\nB: Sir, how much is a glass?\n\nA: P10 for the small one, P15 for the larger one.\n\nB: One small one please. And please put a lot of sago.\n\nAudio Playerhttp://kalyespeak.files.wordpress.com/2010/06/newbs-street_food_taho.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload me!\nGet the PDF!\nPic from this blog.\nSong: Handog by Join the Club\n\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebook7MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-street_food_series_taho.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-street_food_taho.mp3"
  },
  {
    "id": 2,
    "title": "Newbs - Do You have plans tomorrow? Nood tayo ng sine.",
    "content": "Want to ask that cute girl out, but don't quite know how to phrase it in Filipino? Well, here's the lesson for you. Cris and Mickey will teach you how to ask if that pretty morena Pinay has plans for tomorrow. Flash that killer smile along with that perfect Pinoy accent and you may just find yourself scoring a date. The vulture Jovitt joins the duo and gives his observations about first dates.\nA: Sharon, may plano ka ba bukas?\n\nB: Wala naman.\n\nA: Nood tayo ng sine.\n\nB: Sige. Kita-kits.\nA: Sharon, do you have plans for tomorrow?\n\nB: Nope, don't have any.\n\nA: Let's watch a movie.\n\nB: Sure, see you.\n\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-plans_-nood-tayo-ng-sine.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload\n Learn how to boss people around! \nMusic: �Love Team� by The Itchyworms\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": 0,
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-plans_-nood-tayo-ng-sine.mp3"
  },
  {
    "id": 3,
    "title": "Newbs - Asking For A Picture. Isa, Dalawa, Tatlo!",
    "content": "Audio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-asking-for-a-picture-isa-d.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\n\nTraveling around the Philippines alone and dying to have your picture taken with that beautiful Pinay behind you? Don't fret! In this lesson, Cris and Mickey will not only teach you how to charm the locals with your exquisite Filipino, but you'll also learn how to ask someone to take your picture for you! Jovitt gives his two cents on the famous Philippine sunset and where best to see it!\nA: Kuya, pwede papicture?\n\nB: Sige, anong background gusto mo?\n\nA: Umm. kasama yung sunset.\n\nB: Okey! Isa.. dalawa.. tatlo! Smile!\nA: Kuya, can you take my picture?\n\nB: Sure, what background do you want?\n\nA: Umm, with the sunset.\n\nB: Okay! One, two, three! Smile!\nWant to learn how to request for more stuff? Click here\nMusic used:\n\nMaling Akala by Brownman Revival\n\nAkin Ka Nalang by  Itchyworms\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebook4MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-asking_for_a_picture_isa_dalawa_tatlo.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-asking-for-a-picture-isa-d.mp3"
  },
  {
    "id": 4,
    "title": "Newbs - It's broken! Pwede ipapalit?",
    "content": "Ever buy your dream TV, bring it home, set it up in your living room, and just when you're ready to kick-back into LCD-heaven, you find out it doesn't work? If you have, then here's how to say it in Filipino - the simplest way possible. Join Mickey and Cris as they explore the world of brokenness and um, fixed-ness.\nA: Kuya, pwede ipapalit?\n\nB: Bakit?\n\nA: Sira siya eh.\n\nB: Hmm. ayos siya. Hindi lang naka-plug\nA: Kuya, can we have this exchanged?\n\nB: Why?\n\nA: It's broken.\n\nB: Hmm, it's working fine. It just wasn't plugged.\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/04/newbs-its-broken-pwede-ipapalit.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload\nPDF to follow :)\nMusic: George Estregan Groove Explosion by Radioactive Sago Project\n\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": 0,
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-its-broken-pwede-ipapalit.mp3"
  },
  {
    "id": 5,
    "title": "Newbs - Take Out! Pabalot!",
    "content": "Eating out and realized that you ordered too much? That leftover lechon sure is looking good for breakfast tomorrow. But what to do, what to say? Well, in this month's Kspeak lesson, Cris and Mickey will teach you how to say �take out!� faster than you can say �wow, sarap!� Razi, Bernice, and Jovitt also talk about �takaw mata� in the culture portion.\nA: Ang dami pang tira!\n\nB: Pabalot na lang kaya natin?\n\nA: Sige, sabihin mo sa ate!\n\nB: Ate, pabalot!\nA: There's so much leftovers!\n\nB: You think we should take it home?\n\nA: Yeah!! Tell the waitress!\n\nB:  Miss! Please have this wrapped for take out!\nMusic: Tikman by Sugarfree (we don't own the music but we love it!)\n[audio http://kalyespeak.files.wordpress.com/2011/06/newbs-take-out-pabalot1.mp3]\nDownload me!\nGet the PDF here!\nSpread the word! Share us!TwitterEmailFacebook6MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-take-out-pabalot.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-take-out-pabalot1.mp3"
  },
  {
    "id": 6,
    "title": "Newbs-Thief! Saklolo!",
    "content": "You're walking down the street, and next thing you know it, a guy comes running from behind you, snatches your wallet and runs off to the horizon. What are you to do? Shout �Saklolo!� of course! Listen in as the Kspeak Crew talks about this perilously termed Filipino word!\nA: Saklolo! Magnanakaw! Saklolo!\n\nB: Saan siya pumunta?!\n\nA: Doon banda!\n\nB: Ako bahala!\nA: Help! Thief! Help!\n\nB: Where'd he go?\n\nA: Over there!\n\nB: I'll handle it!\nAudio Playerhttp://kalyespeak.files.wordpress.com/2010/08/newbs-thief-saklolo.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\n\nDownload me!\nPDF here!\nMusic: No Touch by Rocksteddy\n\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebook4MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbie-thief_saklolo.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-thief-saklolo.mp3"
  },
  {
    "id": 7,
    "title": "Newbs - I Love You! Pakita Mo .",
    "content": "Ah, love. The four letter word that starts a life of happiness and fulfillment. The four letter word that makes the world go round. The four letter word that makes us gush and see little bunny rabbits hop around with unicorns prancing on colorful rainbows. Um, ok, maybe not the last part. Sadly, it's also the four letter word that causes awfully awkward conversations. such as this one. Listen and learn to profess your love in Filipino with Mickey and Cris.\nA: Pare, alam mo ba? Mahal kita.\n\nB: Huwag mo sabihin iyan. Pakita mo na lang.\nA: Dude, did you know? I love you.\n\nB: Don't say that. just show it.\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-i-love-you-pakita-mo.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload\n Check out a really romantic proposal here! \nMusic: �Kailan� by Smokey Mountain\n\n�This Guy Is In Love With You Pare� by Parokya ni Edgar� and performed by Tryst\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": 0,
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-i-love-you-pakita-mo.mp3"
  },
  {
    "id": 8,
    "title": "Newbs-My Boil! Tara Na Sa Ospital!",
    "content": "You'll know an emergency when you see one - and a festering boil ready to pop is one of them. Listen in as Jovitt and Mickey tackle an emergency in this episode of Kalyespeak. Also, Razi is making his debut as the newest member of the team as the ever-knowledgable Society Snail. This promises to be an insightful lesson of boils, emergencies, useful language, and circumcision (yes, you read it right, circumcision.)\nA: Sasabog na yung pigsa ko!\n\nB: Tara na sa ospital!\n\nA: Bilis ang sakit!\nA: My boil is going to explode!\n\nB: Let's go to the hospital!\n\nA: Hurry! It hurts!!!\n\nAudio Playerhttp://kalyespeak.files.wordpress.com/2010/04/newbs-my_boil_tara_na_sa_ospital.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload me!\nLearn more with this PDF!\nMusic: Superproxy 2K6 by the late great Francis M\n\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebook1MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-my_boil_tara_na_sa_ospital.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-my_boil_tara_na_sa_ospital.mp3"
  },
  {
    "id": 9,
    "title": "Newbs - DVD! Dibidi!",
    "content": "Audio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-dvd-dibidi.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\n\nThat little man whispering to your ear �Dibidi, �Eks, eks�, �buld, buld� might creep you out, but he's actually your friendly DVD bootlegger - source of your pirated DVD needs, whether it be the latest Oscar-award winning film or the skankiest lewd movie. Learn how to haggle with the man with this lesson with Cris and Mickey, and listen to Jov as he gives his insights on the dangers of buying those darn discs.\nA: Dibidi, dibidi! One-fifty, isa!\n\nB: Ang mahal naman! Wala bang presyong kaibigan?\n\nA: Sixty, last price.\n\nB: Ayos! Pabili sampu!\nA: DVD, DVD! One hundred fifty pesos for one!\n\nB: How expensive!, Don't you have a discounted price?\n\nA: Sixty, last price.\n\nB: Ok! I'll buy ten!\n Dying to say �How cheap!� or �That's so long!�? Click here! \nMoozik:\n\n�DVDX� by Sandwich and �Papaya� by Urzula Dudziak\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": 0,
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-dvd-dibidi.mp3"
  },
  {
    "id": 10,
    "title": "Newbs - Yaya, I'm Scared! Mumu!",
    "content": "We go back to the days of yore, where things were simpler, and all our questions were answered by our yayas. Yayas?! Yep, in this episode, Cris and Mickey discuss the Filipino cultural phenomenon called �yayas.' We're not talking about no creepy sisterhood here, but nannies!\nA: Joeyboy! Huwag kang pupunta diyan!\n\nB: Bakit, yaya?\n\nA: May mumu diyan!\n\nB: Takot ako. huhuhu.\nA: Joeyboy! Don't go there!\n\nB: Why, yaya?\n\nA: There's a ghost there!!\n\nB: I'm scared! huhuhu.\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-yaya-im-scared3.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload\n Know more words to scare little kids! Click here! \nMusic: Basagan ng Mukha by Radioactive Sago Project\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-yaya_im_scared.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-yaya-im-scared3.mp3"
  },
  {
    "id": 11,
    "title": "Newbs - Checking the Taxi Meter. Gumagana.",
    "content": "Getting around Manila is easy, but getting around without being cheated by the taxi driver is another matter. In this long-overdue Kalyespeak lesson (sorry for the delay guys!), we'll learn from Mickey and Jovitt how to check if the taxi meter is actually working! Razi and Paulo join in and share their tips on taking a taxi around Manila.\nAudio Playerhttp://kalyespeak.files.wordpress.com/2010/11/newbs-checking-the-taxi-meter-gumagana.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nA: Bos, gumagana ba metro mo?\n\nB: Oo, bakit?\n\nA: Ang mahal kasi eh.\n\nB: Tama yan, traffic eh.\nA: Sir, is your taxi meter working?\n\nB: Yes, why?\n\nA: It's so expensive.\n\nB: That's correct, it's just really traffic.\nDownload me!\nMusic: Slip by Kjwan\nDownload the PDF here!\nSpread the word! Share us!TwitterEmailFacebook3MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-checking_the_taxi_meter-gumagana.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-checking-the-taxi-meter-gumagana.mp3"
  },
  {
    "id": 12,
    "title": "Newbs - I Lost My Wallet! Putek!",
    "content": "You get home one day, excited to show your friend your autograph of Anne Curtis. And then you realize you lost your wallet on the way! Crud! How do you express that in Filipino? Well, listen in as Cris and Mickey teach you a mild swear word, and talk about Anne Curtis.\nA: Putek! Nawawala wallet ko!\n\nB: Ano laman?\nA:P10,000! At yung autograph ni Anne Curtis!\n\nB: Diyos ko!\nA: Crud! I lost my wallet!\n\nB: What was in it?\n\nA: P10,000! And my autograph of Anne Curtis!\n\nB: My God!\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/10/newbs_i-lost-my-wallet-putek.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload!\nWant to be enlightened some more? Well, click here for the PDF!\nWho's Anne Curtis? She's just the leading lady of the Philippines! Search her in Google!\nSong: �Gusto Ko Lamang sa Buhay� by Itchyworms\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebook2MoreRedditLike this:Like Loading...",
    "pdfLink": 0,
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs_i-lost-my-wallet-putek.mp3"
  },
  {
    "id": 13,
    "title": "Newbs - The Curious Case of the Filipino Greeting. Kumain Ka Na Ba?",
    "content": "So, we've learned how to say �kamusta' (or �kumusta' - whichever way you want to spell it), but is there a more Filipino way of greeting people? IS THERE?! Well, indeed there is! Listen in as Cris and Mickey break down this lesson and Jovitt comes in with his four cents on this peculiar Filipino greeting.\nA: Oh, hijo. Kumusta ang biyahe?\n\nB: Ok naman po.\n\nA: Kumain ka na ba?\n\nB: Opo.\nA: Oh, son. How was your trip?\n\nB: It was ok.\n\nA: Have you eaten?\n\nB: Yes sir.\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/08/newbs-the-curious-case-of-the-filipino-greeting-kumain-ka-na-ba_.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload!\nSong: �All For A Tuesday� by Taken By Cars\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebook1MoreRedditLike this:Like Loading...",
    "pdfLink": 0,
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-the-curious-case-of-the-filipino-greeting-kumain-ka-na-ba_.mp3"
  },
  {
    "id": 14,
    "title": "Newbs - Is It Far? Kaunti Na Lang.",
    "content": "Ever had that trip that seemed like forever? Well, we're pretty sure it won't compare to the trip the father and son in this episode are having. hey, there's no longer road than the road in a post-apocalyptic Philippines. Hold the tissue if you have a daddy issue, and listen in as Cris and Mickey brush off the soot and teach you helpful Filipino. Jov keeps on walking with a note for pedestrians out there.\nA: Malayo pa ba, itay?\n\nB: Malapit na anak.\n\nA: Pagod na ako.\n\nB: Kaunti na lang.\nA: Is it still far, dad?\n\nB: It's near already.\n\nA: I'm already tired.\n\nB: Just a little more.\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-is-it-far_-kaunti-na-lang.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload\n\n\nLearn more adjectives here! WOW GRABE! \n\n\n\nOur music for this episode is �Batang Munti� by Narda\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-is_it_far_kaunti_na_lang.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-is-it-far_-kaunti-na-lang.mp3"
  },
  {
    "id": 15,
    "title": "Newbs - Is She In? Anong Oras Siya Babalik?",
    "content": "Audio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-is-she-in_-anong-oras-babalik_.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\n\nEver gone to the house of the girl of your dreams. and realize she's gone? Ever wonder when she's coming back? Ever realize her name was Juliebeth? Well, if you said yes to the first two questions, this is the lesson for you. And if you're wondering if Juliebeth is a real name, then this is ALSO the lesson for you. Join Cris, Mickey and Jov in this lesson and find out when Juliebeth is coming back!\nA: Andiyan po ba si Juliebeth?\n\nB: Ay. wala po.\n\nA: Anong oras po siya babalik?\n\nB: Hindi ko po alam. Baka bukas pa.\nA: Is Juliebeth in?\n\nB: Oh.. she's not here.\n\nA: What time will she be back?\n\nB: I don't know. Probably tomorrow.\n Learn more about Juliebeth here! \nMusic:  Telepono by Sinosikat\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-is_she_in_anong_oras_babalik.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-is-she-in_-anong-oras-babalik_.mp3"
  },
  {
    "id": 16,
    "title": "Newbs - You're Dead! Umuuulan!",
    "content": "Audio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-you_re-dead-umuulan.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume. We only have two seasons here in the Philippines - the hot and the wet. Ok, that just sounded dirty, but you've got the point. And right now, we're smack in the middle of the wet rainy season! So don't forget you're umbrellas.. you never know when the rain's going to pour! Cris and Mickey talk about rain, umbrellas and death, as Jov chimes in with his cultural insights.\nA: Umuulan! Umuulan!\n\nB: Hala! Wala akong payong!\n\nA: Patay ka! Umuulan! Umuulan!\n\nB: Oo na! Oo na! Patay ako! PATAY AKO!\nA: It's raining! It's raining!\n\nB: Oh no! I don't have an umbrella!\n\nA: You're dead! It's raining! It's raining!\n\nB: Yeah! Yeah! I'm dead! I'm dead!\nGet drenched with more knowledge with the supplemental PDFs here!\nOur music: �Superproxy� by The Eraserheads\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-umuulan_umuulan.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-you_re-dead-umuulan.mp3"
  },
  {
    "id": 17,
    "title": "Newbs - Christmas episode! Ano plano natin?",
    "content": "Christmas, ah, that lovely holiday. Although Christmas in the Philippines is different from the Western idea of Christmas - no snowmen or jingle bells here, Pinoys have their own special way of bringing in the cheer. For culture talk, Mickey and Cris discuss different Filipino traditions and share a special Christmas message for all the Kalyespeak supporters.  Merry Christmas everyone! \nA: Nay, ano plano natin sa Pasko?\n\nB: Simbang gabi tapos noche Buena.\n\nA: Kailan tayo magbubukas ng regalo?\n\nB: Pagkatapos ng noche Buena.\nA: Mom, what are we doing for Christmas?\n\nB: Midnight mass, then Christmas dinner.\n\nA: When will we open gifts?\n\nB: After the Christmas dinner.\n\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-christmas-episode-ano-plano-natin_.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload\n Learn more about Filipino Christmas traditions with this merry and jolly PDF! \nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-christmas_episode_ano_plano_natin.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-christmas-episode-ano-plano-natin_.mp3"
  },
  {
    "id": 18,
    "title": "Newbie - I'm Thirsty. Ito, Tubig",
    "content": "Your throat is dry. Your lips are cracked. You're parched, admit it. There's no shame in saying you're thirsty. And because we have no shame here in Kalyespeak, we'll be teaching you how to say �I'm thirsty� in Filipino (talk about a segue!). Tune in as Mickey and Jovitt talk about thirst and water. Listen in closely and you might just guess where Cris G has gone off to!\nA: Nauuhaw ako.\nB: Oh, ito tubig.\nA: Salamat po.\nA: I'm thirsty.\nB: Here, have some water.\nA: Thanks.\nAudio Playerhttp://kalyespeak.files.wordpress.com/2010/03/newbie-im_thirsty_ito_tubig1.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload me!\nWant to learn more? Check out this PDF especially made by Razi!\nMusic: �Lifeline� by Kjwan (galing!)\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebook10MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbie-im_thirsty_ito_tubig.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbie-im_thirsty_ito_tubig1.mp3"
  },
  {
    "id": 19,
    "title": "Newbs - Are you ready? Teka lang.",
    "content": "Aiyo! So you're friend is one of those slow types - 30 minutes to get ready to take a shower, another 30 in the shower, and yet another 30 getting out of the shower. Cris and Mickey give you a lesson on how to ask if he's ready (which he probably isn't). Jovitt flies in to talk about �Filipino time.�\nA: Buboy, handa ka na ba?\n\nB: Hindi pa.\n\nA: Tara na! Late na tayo!\n\nB: Teka lang!\nA: Buboy, are you ready?\n\nB: Not yet.\n\nA: Come on! We're late already!\n\nB: Hold up a bit!\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-are-you-ready_-teka-lang.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload\nPDF to follow!\nMusic: Taralets! by Imago\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": 0,
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-are-you-ready_-teka-lang.mp3"
  },
  {
    "id": 20,
    "title": "Newbs-WIFI? Libre!",
    "content": "Ever go to a coffee shop and feel the need to go online (to listen to the newest episode of kalyespeak, yeh?)?! Well, here's just the lesson for you! Whether you call it wifi or wee-fee, we all need it, so here's how to say it! Join Mickey, Jovitt and Razi as they talk about the language, free wifi and coffee shops\nAudio Playerhttp://kalyespeak.files.wordpress.com/2010/05/newbs-wifi_libre.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload me!\nA: May WIFI ba dito?\n\nB: Siyempre.\n\nA: Ano yung password?\n\nB: Hindi kailangan. Libre eh.\nA: Is there WIFI here?\n\nB: Of course!\n\nA: What's the password?\n\nB: You don't need it because it's free!\nPDF can be found. HERE!\nMusic: Bonggahan by 6CycleMind\n\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\n\nPhoto from Corbis.\nSpread the word! Share us!TwitterEmailFacebook3MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbie-wifi_libre.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-wifi_libre.mp3"
  },
  {
    "id": 21,
    "title": "Newbs-Friendship at its Finest",
    "content": "Ever had those days when you just felt downright ugly? Yeah? Then this lesson is for you! Learn how to be honest with yourself with this lesson hosted by Cris and Mickey! The intricacies and nuances of the Filipino culture is discussed by Bernice, Razi, and Jovitt!\nA: Ang panget ko talaga.\n\nB: Hindi ka panget\n\nA: Salamat. Ang bait mo talaga.\n\nB: Hindi ka lang gwapo.\nA: I'm so ugly!\n\nB: You're not ugly.\n\nA: Thanks, you're so kind.\n\nB: You're just not handsome.\n\nAudio Playerhttp://kalyespeak.files.wordpress.com/2011/04/newbs-friendship_at_its_finest.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\n\nDownload me!\nGet the PDF here!\nMusic: Ako si Superman by Handog\n\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebook20MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-friendship_at_its_finest.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-friendship_at_its_finest.mp3"
  },
  {
    "id": 22,
    "title": "Newbs - I'm from the States. Astig.",
    "content": "Audio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-im-from-the-states-astig.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume. Just because you're white doesn't mean you're from the States. We all know that's true, but give 40 years of colonization plus another 60 years of neo-colonization and you have Filipinos thinking all white folks come from the land of the free. Well, in this lesson, Cris and Mickey teach you how to clarify where you are from, Cris also adds some flavor with his etymology of the word �astig�. Jovitt discusses the importance of hometowns and provinces.\nA: Excuse me po, taga-rito ka ba?\n\nB: Hindi, taga-Amerika ako.\n\nA: Talaga? Taga-Canada ako.\n\nB: Astig.\nA: Excuse me, are you from here?\n\nB: No, I'm from the States.\n\nA: Really? I'm from Canada.\n\nB: Cool.\nClick here for your own PDF\nMusic included in this episode:\n\nDaliri by Kjwan and Princesa by 6Cyclemind\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-im_from_the_states_astig.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-im-from-the-states-astig.mp3"
  },
  {
    "id": 23,
    "title": "Newbs - Does It Go? Iyakin.",
    "content": "That high-pitched androgynous being that is Zsazsa is back, and now she's shopping. Of course, his/her friend is back as well, with a new mean putdown in store for poor Zsazsa. Cris and Mickey explain the language of colors, matching clothes, and Christmas trees (go figure!).\nA: Bagay ba?\n\nB: Pulang t-shirt, green na pantalon. Ano ka - Krismas tree?\n\nA: Ang sama mo.\n\nB: Iyakin.\nA: Does it go?\n\nB: Red shirt, green pants. What do you think you are - a Christmas tree?\n\nA: You're so mean.\n\nB: Crybaby.\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-does-it-go_-iyakin.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload\n Colors and cockroaches in this PDF! Ano ka - sinuswerte?! Of course! \n\n\n\nMusic: �Kung Ayaw Mo Na Sa Akin� by Sugarfree\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-does_it_go_iyakin.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-does-it-go_-iyakin.mp3"
  },
  {
    "id": 24,
    "title": "Newbs - It's so Hot! Arte mo!",
    "content": "Audio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-its-so-hot-arte-mo.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\n\nThe vicious heat of the Filipino summer getting to you? It's not all about the tropical paradise of beaches, bikini-clad Pinays, and cold beer you know. For the rest of us who aren't at the beach, we have to endure scorching 37 degree temperatures, which can lead to �inis init�. Jovitt discusses this tropical phenomenon while Cris and Mickey take a looksy at a  dialogue which can go �either way.�\nA: Ang init ngayon! Natutunaw ako!\n\nB: Exage ka naman! Hindi kaya.\n\nA: Bakit ka ganyan?\n\nB: Ang arte mo eh.\nA: It's so hot today! I'm melting!\n\nB: You're exaggerating! It's not THAT hot.\n\nA: Why do you have to be that way? :(\n\nB: Because you're so fussy.\n The UPDATED PDF is just a click away!\nMusicalized by:\n\n�Paris� by Chicosci and �Hallelujah� by Bamboo\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebook3MoreRedditLike this:Like Loading...",
    "pdfLink": 0,
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-its-so-hot-arte-mo.mp3"
  },
  {
    "id": 25,
    "title": "Newbs - Adobo. AGAIN?!",
    "content": "Ever got home craving for a double cheese burger and all you got was a crummy salad? Ever happen to you twice. thrice. quatrice (there such a word?)??? Well, in this lesson, Cris and Mickey (who both sport a weird English accent) will teach you how to show disappointment, albeit in a rude way. Jovitt pipes in with his recipe for the delectable Filipino dish - adobo.\nA: Ano ulam natin?\n\nB: Adobo po.\n\nA: Adobo na naman??? Sawa na ako sa adobo!!!\nA: What food are we having?\n\nB: Adobo.\n\nA: Adobo AGAIN??? I'm sick and tired of adobo!!!\nAudio Playerhttp://kalyespeak.files.wordpress.com/2009/05/newbs-adobo-again_.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload\n Want to learn how to say you're sick and tired of being sooooo good? Check this PDF lesson file out! \n Want to make your own adobo? Click here for a basic recipe! \nDo you have your own adobo recipe? Leave it on the comments section! :)\n\nMusic � Butterscotch by The Eraserheads\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebook4MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-adobo_again.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-adobo-again_.mp3"
  },
  {
    "id": 26,
    "title": "Newbs - Street Food Series: This is Delicious! Balut!",
    "content": "People say Filipinos eat anything. And, honestly, maybe we do. In the pilot lesson of the new Street Food Series, the Kalyespeak crew will peel away the shell of everyone's favorite egg - Balut. What is it? Is it good? Is that a feather?! Find out the answers here!\nA: Ang sarap nito!\n\nB: Ano ba iyan?\n\nA: Balut!\n\nB: Wow! Pahingi!\nA: This is delicious!\n\nB: What is that?\n\nA: Balut!\n\nB: Wow! Give me some!\nAudio Playerhttp://kalyespeak.files.wordpress.com/2010/05/newbs-street_food_series_this_is_delicious_balut.mp300:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.\nDownload me!\nGet the PDF here!\nMusic: Healing by Julianne\n\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\n \nPicture thanks to Corbis.\nSpread the word! Share us!TwitterEmailFacebook13MoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-street_food_series_this_is_delicious_balut.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-street_food_series_this_is_delicious_balut.mp3"
  },
  {
    "id": 27,
    "title": "Newbs - I'll Go Ahead. Ingat!",
    "content": "Audio Player00:0000:0000:00Use Up/Down Arrow keys to increase or decrease volume.http://kalyespeak.files.wordpress.com/2009/05/newbs-ill-go-ahead-ingat.mp3We say goodbye to you today. okay okay, don't fret. We're TEACHING you how to say goodbye today. In this short but sweet lesson, Cris and Mickey will teach you some ways to say goodbye to a friend or make a smooth getaway from an annoying person. Jovitt hops on the culture trail and talks about the Filipino text messaging culture.\nA: Una na ako pare!\n\nB: Sige, text-text na lang!\n\nA: Okey, kita-kits!\n\nB: Ingat!\nA: I'll go ahead mate.\n\nB: Sure, just text me!\n\nA: Okay, see you!\n\nB: Take care!\n Learn more about saying goodbye in this PDF! \n\nMusic used in this episode:\n\nRock Baby Rock by Kala, and One Look by Kjwan.\nNo copyright infringement intended. If you like the music, go and support OPM by buying the song legally online or in your nearest music store!\nSpread the word! Share us!TwitterEmailFacebookMoreRedditLike this:Like Loading...",
    "pdfLink": "/storage/sdcard1/Tagalog/Newbies/newbs-ill_go_ahead_ingat.pdf",
    "mp3Link": "/storage/sdcard1/Tagalog/Newbies/newbs-ill-go-ahead-ingat.mp3"
  }
]