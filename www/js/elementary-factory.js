App.factory('ElementaryFactory', function ElementaryFactory() {
    var posts = [{
        "id": 0,
        "title": "Elementary-We’re Late. Maliligo Lang Ako.",
        "content": "You’re in a rush, you have  to get going, but your friend stinks. What to do?  Well, practice this helpful dialog of course! Mickey and Jovitt break down the useful phrases of this episode, while Pupay and Razi talk about palangganas, tabos, and taking a shower!<br /><blockquote>A: Pare, huli na tayo!<br />B: Maliligo lang ako nang mabilis!<br />A: Ano? Huli na tayo!<br />B: Alam ko! Kaso, ang baho ko eh!<br /><div class='section-heading'>Translation:</div>A: Dude, we’re late!<br />B: I’ll just take a quick shower!<br />A: Whut?! We’re late!<br />B: I know! But I stink!</blockquote>",
        "pdfLink": "elementary-were_late_maliligo_lang_ako.pdf",
        "mp3Link": "elementary-were-late-maliligo-lang-ako.mp3",
        "img": null
    }, {
        "id": 1,
        "title": "Elementary – Where Does It Hurt? Sa Puso.",
        "content": "We head to the doctor’s office today, but as we shall find out, the hurt of this patient needs more than just medical care. It’s a hurt that’s deep, and it does not scar – and that’s the worst hurt that one can get. Mickey and Jovitt tackle the language. They’re joined in by Razi and Paulo who will introduce us to different body parts and how to say them in Filipino.<br /><blockquote>A: Saan masakit? Sa ulo ba? Sa tiyan?<br />B: Hindi po, doc. Sa puso.<br />A: Heart attack iyan!<br />B: Hindi po. Heartbreak.<br /><div class='section-heading'>Translation:</div>A: Where does it hurt? In your head? Your stomach?<br />B: No, doctor. It’s my heart.<br />A: That’s a heart attack!<br />B: No, it’s heartbreak.</blockquote>",
        "pdfLink": "elementary-where_does_it_hurt_sa_puso.pdf",
        "mp3Link": "elementary-where_does_it_hurt_sa_puso.mp3",
        "img": "elementary/masakit-copy.jpg"
    }, {
        "id": 2,
        "title": "Elementary – Pick-up Line Fail",
        "content": "Hankering for some loving? Then how about try a Filipino pick-up line? Just don’t follow this girl as she does an epic fail. It’s also a special episode because we’re joined by a special guest and one of the Kspeak founders is back! Listen in for this extra espesyal lesson!<br /><blockquote>A: Boss, alam mo ba? Kung posporo ka, at posporo ako, eh di match tayo!<br />B: Nye.<br />A: Ah.. umm.. umutot ka ba? Kasi you blew me away!<br />B: Ano?!<br /><div class='section-heading'>Translation:</div>A: Sir, did you know? If you were a matchstick, and I were a matchstick, we’d match!<br />B: Meh.<br />A: AH.. umm.. did you fart? Coz you blew me away!<br />B: What?!?</blockquote>",
        "pdfLink": null,
        "mp3Link": "elementary-pickup-line-fail.mp3",
        "img": "elementary/pickup-line-fail.jpg"
    }, {
        "id": 3,
        "title": "Elementary – They Were Kissing. Walang-hiya siya! (The Jun-Bing-Mak Love Triangle)",
        "content": "Ever had your heart broken by a two-timing loved one? If you haven’t, you can live through the excruciating ordeal vicariously as you listen to a boyfriend’s painful realization that his heart just got chopped in two. If you have, well, um, time to get over it. In this unusually long but entertaining episode, Cris and Mickey leave out the anger as they delve into the nitty-gritty of the dialog, while the culture vulture Jovitt takes a bite out of Filipino names.<br /><blockquote>A: Jhunjhun, girlfriend mo si Bingbing, di ba?<br />B: Oo, bakit?<br />A: Nakita ko siya kasama si Makmak. Naghahalikan sila.<br />B: ANO?! Walang-hiya siya.<br />A: Relax lang…<br />B: Saan yung bolo ko?!<br /><div class='section-heading'>Translation:</div>A: Jhunjhun, Bingbing is your girlfriend, right?<br />B: Yeah, why?<br />A: I saw her with Makmak. They were kissing.<br />B: What??? That b*tch!<br />A: Just relax…<br />B: Where’s my machete?!</blockquote>",
        "pdfLink": "elementary-they_were_kissing_walang_hiya.pdf",
        "mp3Link": "elementary-they-were-kissing-walang-hiya-siya.mp3",
        "img": null
    }, {
        "id": 4,
        "title": "Elementary – Pepe Gets His Wish. Amoy Aso.",
        "content": "Whoopee, it’s Pepe’s birthday and he finally gets his wish! But little does he know that it’s actually his DEATH wish. We like it morbid here in KalyeSpeak so it isn’t surprising that our little ol’ Nanay finds Pepe sprawled on the floor. Cris and Mickey get into the details of the sad (but interesting) story and Jov chimes in with his two cents on Filipinos and their olfactory fixations.<br /><blockquote>A:Pepe, Pepe! Andito na si Nanay!<br />Hmm, bakit amoy aso?<br />Anak, saan nanggaling iyan??<br />B: Naaay.. salamat po..<br /><div class='section-heading'>Translation:</div>A: Pepe, Pepe! Mother’s home!<br />Hmm.. Why does it smell of dog?<br />My child! Where did that comes from?<br />B:: Ma.. Thank you…<br /> Want to learn how to say “This smells like crap!”?</blockquote>",
        "pdfLink": "elementary-pepe_gets_his_wish_amoy_aso.pdf",
        "mp3Link": "elementary-pepe-gets-his-wish-amoy-aso.mp3",
        "img": null
    }, {
        "id": 5,
        "title": "Elementary – Pepe wants a dog. Ayoko.",
        "content": "Why do we always want something we can’t have? Honestly, we don’t know, but it sure makes a good story. In the first Elementary lesson, Cris and Mickey take a look at the life of little Pepe, his forbidden desire, and how it’s starting to rip apart his relationship with a loved one. Sounds a tad bit too dramatic? Well, that’s how we like it. Jovitt adds to the hoolah with an insider’s look at Filipinos and their pets.<br /><blockquote>A: Nay, gusto ko ng aso.<br />B: Pepe, hindi nga pwede.<br />A: Bakit, nay? Bakit???<br />B: Basta. Ayoko.<br /><div class='section-heading'>Translation:</div>A: Ma, I want a dog.<br />B: You can’t have any, Pepe.<br />A: Why, ma? Why???<br />B: Because I said so. That’s why.</blockquote>",
        "pdfLink": "elementary-pepe_wants_a_dog-_ayoko.pdf",
        "mp3Link": "elementary-pepe-wants-a-dog-ayoko.mp3",
        "img": "elementary/aso.jpg"
    }, {
        "id": 6,
        "title": "Elementary – You Stink, Kilikili!",
        "content": "You know you and a friend are tight when conversations touch on the following topics:<br />1. love (and the problems which go with it),<br />2. lifelong dreams (and how to realize them), and<br />3. stinky armpits (and the use of deodorant).<br />We’ll leave you to figure out how to discuss the first two in Filipino. In this Elementary lesson, Cris and Mickey teach you how to put all tact behind with a lesson on the verb “gamit” and of course, armpits. Yup, kilikili. Jovitt finally puts some pants on and talks about Filipino Hygiene.<br /><blockquote>A: My Gad! Ang baho ng kilikili mo!<br />B: Talaga?<br />A: Sobra! Gumamit ka ba ng deodorant?<br />B: Oo, Gumamit ako!<br />A: Eh bakit ang baho?<br /><div class='section-heading'>Translation:</div>A: Oh my god! Your armpits stink!<br />B: Really??<br />A: Super! Did you use deodorant?<br />B: Yea, I did use!<br />A: Why does it stink then??</blockquote> Learn more um-structured verbs with this PDF!",
        "pdfLink": null,
        "mp3Link": "elementary-you-stink-kilikili.mp3",
        "img": null
    }, {
        "id": 7,
        "title": "Elementary – At the KTV. Huwag kang makulit!",
        "content": "Filipinos love to sing, that’s why there’s a million KTV hotspots around the Metro. Some are family-oriented, some are for friends looking for a good-time, and some are for those lonely folk looking for a “good-time”. Go where you want, we here in Kspeak won’t judge you, just as long as you learn the language. Cris and Mickey lower the volume of the screeching tunes of the neighboring KTV-fest and get down with the grammar. Jovitt joins in and belts a tune of his own.<br /><blockquote>A: Ang galing ko kumanta! Ikaw naman!<br />B: Ayoko!<br />A: Sige na! Kanta ka na!<br />B: Ayoko nga eh!<br />A: Sige na! Isang kanta lang!<br />B: Huwag kang makulit!<br /><div class='section-heading'>Translation:</div>A: I sing so well! Your turn!<br />B: I don’t want to!<br />A: Come on! Sing!<br />B: I told you I don’t want to!<br />A: Come on! Just one song!<br />B: Stop pestering me!</blockquote>",
        "pdfLink": "elementary-at_the_ktv_huwag_kang_makulit.pdf",
        "mp3Link": "elementary-at-the-ktv-huwag-kang-makulit.mp3",
        "img": null
    }, {
        "id": 8,
        "title": "Elementary – Lilipat Migration",
        "content": "Saying goodbye is never easy, but remembering always softens the blow. In this new lesson from the Kspeak Crew, we talk something every Filipino can relate to – migration! We will also learn how easy it is to use the future tense in Filipino! Kinig na!<br />A: Lilipat kami papuntang States.<br/>B: Hah? Kailan kayo aalis?<br/><blockquote>A: Bukas na.<br/>B: Ay. Huwag mo akong kalimutan ah.<br/><div class='section-heading'>Translation:</div>A: We’ll be moving to the States.<br/>B: What?! When are you leaving?<br/>A: Tomorrow.<br/>B: Oh.. please don’t forget me…</blockquote>",
        "pdfLink": null,
        "mp3Link": "elementary-lilipat-migration.mp3",
        "img": "elementary/dscn0994.jpg"
    }, {
        "id": 9,
        "title": "Elementary – Picky Eater. Bahala ka!",
        "content": "Game for a night out trying out Filipino cuisine? Can’t describe that tingling sensation on your tongue? Well, listen in on this date as it goes from sweet to sour… pun intended. Cris and Mickey eavesdrop on a night out as we learn the different tastes of Filipino cuisine.<br /><blockquote>A: Gusto mo ba ng sinigang?<br />B: Ayoko. Maasim masyado.<br />A: Eh bicol express?<br />A: Ayoko. Maanghang masyado.<br />B: Eh mangga?<br />A: Ayoko. Matamis masyado.<br />B: Ay, bahala ka.<br /><div class='section-heading'>Translation:</div>A: Do you want sinigang?<br />B: Nah, too sour.<br />A: How about bicol express?<br />B: Nah, too spicy.<br />A: How about a mango then?<br />B: Nah, too sweet.<br />A: Ah, whatever. Eat what you want. Suit yourself.</blockquote>",
        "pdfLink": "elementary-picky_eater_bahala_ka.pdf",
        "mp3Link": "elementary-bahala-ka.mp3",
        "img": "elementary/bahala-ka.jpg"
    }, {
        "id": 10,
        "title": "Elementary – Bababa ba? Baaaah…",
        "content": "Ever got trapped with a Filipino in the elevator? Well, here’s the perfect thing to say to break the awkward silence. Jovitt covers for Cris and joins Mickey as they talk about a useful Filipino tongue-twister and give more examples that will surely get you saying… “huuuuwatttt???”<br /><div class='section-heading'>Dialogue:</div><blockquote>A: Papa, bababa ba?<br />B: Oo, bababa. Bababa ka ba?<br />A: Oo, bababa ako kasama yung tupa.<br />Tupa: Beeeeeeeh.. Beeeeeeeeh…<br /><div class='section-heading'>Translation:</div>A: Papa, is it going down?<br />B: Yup, it’s going down. Are you going down?<br />A: Yup, I’m going down, and I have the sheep with me.<br />Sheep: Beeeh… beeehh..<br />Get down, get down and move it all around with the PDF guide!</blockquote>",
        "pdfLink": "elementary-tongue_twisting_madness.pdf",
        "mp3Link": "newbs-bababa-baaah.mp3",
        "img": null
    }, {
        "id": 11,
        "title": "Elementary – Who Are You Voting For? Yung Pinaka…",
        "content": "Colorful and tacky posters, annoying jingles, fake smiles and insincere handshakes – it’s that time again in the Philippines – National Elections! Listen in as Jov and Mickey talk about the upcoming May 2010 National Elections as they teach some helpful election-related language! So, who are you voting for?<br /><blockquote>A: Sino iboboto mo?<br />B: Yung pinakamatalino. Ikaw?<br />A: Yung pinakamasipag.<br /><div class='section-heading'>Translation:</div>A: Who are you voting for?<br />B: The smartest one. You?<br />A: The most hardworking one.</blockquote>",
        "pdfLink": "elementary-who_are_you_voting_yung_pinaka.pdf",
        "mp3Link": "elementary-who_are_you_voting_for_yung_pinaka.mp3",
        "img": "elementary/kspeakelection.jpg"
    }, {
        "id": 12,
        "title": "Elementary-Lover’s Spat. Honeybabes.",
        "content": "We’ve all been through a quarrel or a spat with a loved one. And we’ve always had our own techniques and tricks of patching things up. Now, it’s time to learn how to patch things up the Filipino way. Mickey and Jovitt teach the language, while Razi and Paulo join in for some insights on “tampo” and “lambing.”<br /><blockquote>A: Oh, nagtatampo ka na naman.<br />B: Ikaw kasi eh. Nangchichicks ka na naman.<br />A: Honeybabes, friends lang kami. Promise.<br />B: Talaga lang ha.<br /><div class='section-heading'>Translation:</div>A: Hey, you’re mad at me again…<br />B: It’s your fault. You’ve been flirting with girls again.<br />A: Honeybabes, we’re just friends. Promise.<br />B: Really now…</blockquote>",
        "pdfLink": "elementary-lovers_spat_honeybabes.pdf",
        "mp3Link": "elementary-lovers_spat_honeybabes.mp3",
        "img": "elementary/tampo-copy.jpg"
    }, {
        "id": 13,
        "title": "Elementary – I’m Lost. Kailangan Ko.",
        "content": "You think you’re one place, turns out you’re miles away. Maybe you hit a wormhole or something. If ever you get trapped in a twilight zone and end up in the Philippines, you’ll learn just exactly what to say in this episode. Mickey and Cris take on the curious incident of the Cebuano lost in Manila.<br /><blockquote>A: Waaah! Waaah!<br />B: Oh, bata, anong nangyari?<br />A: Kailangan ko ng tulong. Nawawala ako. Nasaan ba ako?<br />B: Nasa Manila tayo, boy.<br />A: Talaga? Kanina lang nasa Cebu ako.<br />B: Ano?! Paano nangyari iyon?<br /><div class='section-heading'>Translation:</div>A: Waah! Waah!<br />B: Hey kid, what happened?<br />A: I need help. I’m lost. Where am I?<br />B: We’re in Manila, kid.<br />A:  Really? I was in Cebu just a moment ago!<br />B: What?! How’d that happen?</blockquote>",
        "pdfLink": "elementary-im_lost_kailangan_ko.pdf",
        "mp3Link": "elementary-im-lost-kailangan-ko.mp3",
        "img": "elementary/lost.jpg"
    }, {
        "id": 14,
        "title": "Elementary – Lakbay Tayo! Boracay",
        "content": "Manila is the capital of the Philippines… but Boracay is, um, the CAPITAL of FUN. Ok, that sounded lame. But you get the point – Boracay is the place to go to for beach fun and partying. Listen in as Cris and Mickey give Jovitt a call, as the culture vulture enjoys the view in Boracay.<br /><blockquote>Andito ako ngayon sa Boracay!<br />Ang ganda ng beach!<br />Parang pulbos yung buhangin, at ang linaw ng tubig!<br />Ang dami pang naka-bikini! Wow!<br />Kaso ang dami ring naka-Speedo! Kadiri.<br /><div class='section-heading'>Translation:</div>I’m here right now in Boracay!<br />The beach is awesome/beautiful!<br />The sand is like powder!<br />And the water is so clear!<br />And there’s also a lot of people in bikinis! Wow!<br />But, there are also a lot in Speedos. Yuck.</blockquote>",
        "pdfLink": "elementary-lakbay_tayo_boracay.pdf",
        "mp3Link": "elementary-jovitt-in-boracay.mp3",
        "img": "elementary/borapic.jpg"
    }, {
        "id": 15,
        "title": "Elementary – Why Doesn’t She Love Me? Loser Ka Eh.",
        "content": "We all get down because of that elusive and slippery four-letter word we call “love.” Why doesn’t he care, why doesn’t she love me, what’s wrong with me? These are the questions we are usually faced with. And, sadly, the answers are often too hard to swallow. Listen in as a heartbroken gent receives the ugly truth from his friend. Jovitt and Mickey handle the teaching, while Razi comes in with his two cents on love and the world of emo.<br /><blockquote>A: Bakit hindi niya ako mahal?<br />B: Loser ka eh.<br />A: Bakit ka ganyan?<br />B: Emo ka eh.<br /><div class='section-heading'>Translation:</div>A: Why doesn’t she love me?<br />B: Coz you’re a loser.<br />A: Why are you like that?<br />B: Coz you’re emo.</blockquote>",
        "pdfLink": "elementary-why_doesnt_she_love_me_emo.pdf",
        "mp3Link": "elementary-why_doesnt_she_love_me_emo.mp3",
        "img": "elementary/emo_by_lucky_loser-copy.jpg"
    }, {
        "id": 16,
        "title": "Elementary – Balita ko… It’s Dangerous. (The Pepe Saga)",
        "content": "We heard Pepe and his hysterical “bakit, nay, bakit?”. We heard his mother shutting him up with a single “basta.” But we never found out the real reason… until now. The mystery unravels as the plot thickens in the dramatic story that is Pepe’s dog. Cris and Mickey explain the finer details, while Jovitt joins in to explain the power relations in the Filipino household.<br /><blockquote>A: Mahal, balita ko gusto ni Pepe ng aso.<br />B: Mahirap iyan. May asthma yung bata. Delikado.<br />A: Alam ko. Pero birthday niya…<br />B: Hindi nga pwede eh!<br /><div class='section-heading'>Translation:</div>A: Love, i heard Pepe wants a dog.<br />B: That’s going to be hard. The kid has asthma. It’s dangerous.<br />A: I know.. But it’s his birthday..<br />B: I told you, it ain’t happening.<br />Click here for more learning goodness!<br />Thanks to Raissa!<br />Salamat sa Musika!</blockquote>",
        "pdfLink": "elementary-balita_ko_its_dangerous.pdf",
        "mp3Link": "elementary-pepe-wants-a-dog-part-2.mp3",
        "img": "elementary/quarrel1.jpg"
    }, {
        "id": 17,
        "title": "Elementary – It Wasn’t Me! Akala ko… (The Jhun-Bing-Mak Love Triangle II)",
        "content": "We love confrontations, and this is the confrontation to end all confrontations. Jhunjhun (with bolo in hand) finally catches up with Bingbing and they have a heated talk on the alleged kissy-kissy with Makmak. Cris and Mickey listen in as an unexpected twist unfolds.<br /><blockquote>A: Bingbing! Kasama mo raw si Makmak?!<br />B: Hah? Hindi ako iyon!<br />A: Nakakasuka ka. Bruha.<br />B: Sinasabi ko sa iyo… HINDI AKO IYON!<br />A: Kung hindi ikaw, sino?!<br />B: Si… si Bengbeng, yung kambal ko. Akala ko… akala ko patay na siya.<br /><div class='section-heading'>Translation:</div>A:Bingbing! I heard you were with Makmak?!<br />B: Huh? That wasn’t me!<br />A: You make me vomit. Witch.<br />B: I’m telling you! It wasn’t me!<br />A: If it wasn’t you, who was it then?<br />B: It was… it was Bengbeng, my twin. I thought… I thought she was dead!</blockquote>",
        "pdfLink": null,
        "mp3Link": "elementary-it-wasn_t-me-akala-ko.mp3",
        "img": null
    }, {
        "id": 18,
        "title": "Elementary – Suntanned from the Beach",
        "content": "It’s not a Christmas episode, we know, but who wouldn’t want a lesson about the beach? Well, here it is! Mickey and Cris talk about getting a tan from the beach! And be careful how you pronounce that B word (unless you want to emulate our favorite VJ)! The lesson will also talk about focus verbs (oooh, exciting!! Razi, Jov, and Paulo join in and give a rundown of our favorite beaches!<br /><blockquote>A: Ba’t ang itim mo?<br />B: Pumunta ako sa beach kahapon.<br />A: Ano ginawa mo doon?<br />B: Lumangoy, ano pa ba?<br /><div class='section-heading'>Translation:</div>A: Dude, why do you seem so dark?<br />B: I went to the beach yesterday.<br />A: What did you do there?<br />B: I swam, what else?</blockquote>",
        "pdfLink": null,
        "mp3Link": "elementary-suntanned-from-the-beach1.mp3",
        "img": null
    }, {
        "id": 19,
        "title": "Elementary – Street Food Series: Fishballs!",
        "content": "We are back! And what better way to usher in 2011 than with a lesson on food! Cris and Mickey talk about this uber-famous Filipino street food, fishballs! Paulo and Jovitt explain how and where to buy these yummy balls-on-a-stick, and guest Bernice also shares her two centavos! Listen now, and don’t forget to listen until the end for a musical treat by Cris!<br /><blockquote>A: Wow Fishballs! Ano yung masarap na sawsawan?<br />B: May matamis, may maanghang. Depende na sa iyo yan!<br />A: Sige, yung matamis na lang!<br /><div class='section-heading'>Translation:</div>A: Wow Fishballs! What’s a good sauce?<br />B: There’s some sweet sauce, there’s some spicy. It all depends on you!<br />A: Ok, i’ll just get the sweet one then!</blockquote>",
        "pdfLink": null,
        "mp3Link": "elementary-streetfood-series-fishballs.mp3",
        "img": "elementary/fishballs.png"
    }, {
        "id": 20,
        "title": "Elementary – Joke Time!",
        "content": "Is there a better way than to start your day with a joke? Of course! That’s by saying a joke in Filipino! In this (long-awaited!) installment of Kalyespeak, Cris and Mickey share a classic Filipino joke, while Jovitt gets off his pison and shares some insights on Filipino jokes.<br /><blockquote>A: May joke ako! Anong tawag sa taong nasagasaan ng pison?<br />B:  Ano?<br />A: Tanga!<br /><div class='section-heading'>Translation:</div>A: I have a joke! What do you call a person who gets run over by a steam roller?<br />B: What?<br />A: Stupid!</blockquote>",
        "pdfLink": "elementary-joke-time.pdf",
        "mp3Link": "elementary-joke-time.mp3",
        "img": "elementary/joke-time-copy.jpg"
    }, {
        "id": 21,
        "title": "Elementary – Gutom! Sisig!",
        "content": "After a long wait, we’re back with, yes, you guessed it, another food-related lesson! Cris, Jov, and Mickey tackle a classic Pampangueno dish: sisig – a staple for almost every Filipino who drinks beer or hangs out in a grill. It’s delicious. It’s succulent. It’s pig’s head. And it’s making us hungry.<br /><blockquote>A: Gutom na ako!<br />B: Maghintay ka muna!<br />A: Bakit ang tagal?<br />B: Hindi pa luto yung sisig!<br />A: AAARGH! GUTOM NA AKO!<br /><div class='section-heading'>Translation:</div>A:I’m hungry!!!<br />B: Just wait a while!<br />A: What’s taking so long?<br />B: The sisig isn’t cooked yet!<br />A: ARGH! I’M HUNGRY!</blockquote>",
        "pdfLink": "elementary-gutom_sisig.pdf",
        "mp3Link": "elementary-gutom-sisig.mp3",
        "img": "elementary/sisig.jpg"
    }, {
        "id": 22,
        "title": "Elementary – Driving School",
        "content": "They say that if you can drive in the Philippines, you can drive anywhere. But this innate talent of driving has to come from somewhere right? Of course! And all roads lead back to driving school which is what Mickey and Cris will discuss in 2012’s first Kalyespeak episode.<br /><blockquote>A: Marunong ka ba magmaneho?<br />B: Opo!<br />A: Sige, ano yang hawak mo?<br />B: Cambio po.<br />A: Sira! Manibela yan!<br /><div class='section-heading'>Translation:</div>A: Do you know how to drive?<br />B: Yes!<br />A: Ok, so what’s that thing your holding?<br />B: The gear stick.<br />A: Crazy! That’s the steering wheel!</blockquote>",
        "pdfLink": "elementary-driving_school.pdf",
        "mp3Link": "elementary-driving-school.mp3",
        "img": null
    }, {
        "id": 23,
        "title": "Elementary-Do You Want to Talk? May Problema Ako.",
        "content": "We’ve all had our fair share of ups and downs. That’s just how life goes. And we all know we couldn’t have made it without the help of our friends. This episode shows exactly the opposite of those times. Listen in as Mickey and Jov eavesdrop on a heart-to-heart talk of two “friends.” Razi and Paulo join in and talk about the Filipino cultural phenomenon known as “utang ng loob.”<br /><blockquote>A: May problema ako.<br />B: Gusto mo bang pag-usapan?<br />A: May pinatay ako.<br />B: Ah.. sige… una na ako.<br /><div class='section-heading'>Translation:</div>A: I have a problem.<br />B: Do you want to talk about it?<br />A: I killed someone.<br />B: Oh… all right… I have to go…</blockquote>",
        "pdfLink": "elementary-do-you-want-to-talk-may-problema-ako.pdf",
        "mp3Link": "elementary-do-you-want-to-talk-may-problema-ako.mp3",
        "img": "elementary/pagusapan-copy.jpg"
    }];
    return posts;
});
